package time;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest {

	@Test
	public void testGetTotalMilliseconds() {
		int totalMs = Time.getTotalMilliseconds("12:05:05:05");
		assertTrue("Invalid number of milliseconds", totalMs == 5);
	}

	@Test(expected = NumberFormatException.class)
	public void testGetTotalMillisecondsException() {
		int totalMs = Time.getTotalMilliseconds("12:05:05:0W");
		fail("Invalid number of milliseconds");
	}
	
	@Test
	public void testGetTotalMillisecondsBoundaryIn() {
		int totalMs = Time.getTotalMilliseconds("12:05:05:999");
		assertTrue("Invalid number of milliseconds",totalMs==999);
	}

	@Test(expected = NumberFormatException.class)
	public void testGetTotalMillisecondsBoundaryOut() {
		int totalMs = Time.getTotalMilliseconds("12:05:05:1000");
		fail("Invalid number of milliseconds");
	}
	
	@Test
	public void testGetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		assertTrue("The second provided does not match the result", totalSeconds == 3661);
	}

	@Test(expected = NumberFormatException.class)
	public void testGeTotaltSecondsBoundaryException() {
		int totalSeconds = Time.getTotalSeconds("01:01:0a");
		assertFalse("The second provided does not match the result", totalSeconds == 3661);
	}

	@Test
	public void testGetTotalSecondsBoundaryIn() {
		int totalSeconds = Time.getTotalSeconds("01:01:59");
		assertTrue("The second provided does not match the result", totalSeconds == 3719);
	}

	@Test(expected = NumberFormatException.class)
	public void testGetTotalSecondsBoundaryOut() {
		int totalSeconds = Time.getTotalSeconds("01:01:60");
		assertFalse("The second provided does not match the result", totalSeconds == 3720);
	}
}
